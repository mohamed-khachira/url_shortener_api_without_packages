# URL Shortener

![](RX.png)

## Tools

- PHP 7.4
- Symfony 4.4
- MySQL 8
- Nginx web server
- Vue.js 2
- Webpack Encore

## Installation

```
# git clone https://gitlab.com/mohamed-khachira/url_shortener_api_without_packages.git

# cd url_shortener_api_without_packages

# make up

# docker-compose exec php74-service composer install

# docker-compose exec php74-service php bin/console doctrine:database:create

# docker-compose exec php74-service php bin/console doctrine:schema:update --force

# docker-compose exec php74-service php bin/console doctrine:fixtures:load --no-interaction

# docker-compose exec php74-service npm install

# docker-compose exec php74-service npm run dev

```

## Unit tests && Functional tests

```
# docker-compose exec php74-service php bin/console doctrine:database:create --env=test

# docker-compose exec php74-service php bin/console doctrine:schema:update --force --env=test

# docker-compose exec php74-service php bin/console doctrine:fixtures:load --no-interaction --env=test

# docker-compose exec php74-service bin/phpunit tests/Unit/

# docker-compose exec php74-service bin/phpunit tests/Func/

```

## Usage

To log in to the Dashboard, you can use these credentials:

| Email                  | Password                      
| ---------------------- | ----------------------------- 
| default-user@test.de   | default-password 

You could also create your own account.

## Useful links

| App url                | API url                       | Adminer url(MySQL)    |
| ---------------------- | ----------------------------- | --------------------- |
| http://localhost:8080/ | http://localhost:8080/api/doc | http://localhost:8085 |
