import Vue from "vue";
import App from "./components/App";
import router from "./router/main";
import store from "./store/main";
import axios from "axios";
import Vuelidate from "vuelidate";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";

Vue.use(BootstrapVue, IconsPlugin);
Vue.use(Vuelidate);

//axios.defaults.baseURL = "http://127.0.0.1:8000/api";
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL + "/api";

require("./store/subscriber");

store.dispatch("auth/attempt", localStorage.getItem("token")).then(() => {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount("#root");
});
