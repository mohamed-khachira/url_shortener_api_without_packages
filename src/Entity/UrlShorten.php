<?php

namespace App\Entity;

use App\Repository\UrlShortenRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UrlShortenRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class UrlShorten
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"urls_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The URL is required !")
     * @Assert\Url(message = "The url '{{ value }}' is not a valid url !")
     * @Groups({"urls_read"})
     */

    private $fullUrl;

    /**
     * @ORM\Column(type="string", length=7)
     * @Assert\NotBlank(message="Tokenized URL is required !")
     * @Assert\Type(type="alnum", message="The tokenized URL must be stored in a 7 character, alphanumeric string !")
     * @Assert\Length(min=7, max=7, exactMessage="This value should have exactly {{ limit }} characters.")
     * @Groups({"urls_read"})
     */
    private $shortUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"urls_read"})
     */
    private $clicks = 0;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Groups({"urls_read"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="urlShortens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullUrl(): ?string
    {
        return $this->fullUrl;
    }

    public function setFullUrl(string $fullUrl): self
    {
        $this->fullUrl = $fullUrl;

        return $this;
    }

    public function getShortUrl(): ?string
    {
        return $this->shortUrl;
    }

    public function setShortUrl(string $shortUrl): self
    {
        $this->shortUrl = $shortUrl;

        return $this;
    }

    public function getClicks(): ?int
    {
        return $this->clicks;
    }

    public function setClicks(?int $clicks): self
    {
        $this->clicks = $clicks;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtAutomatically()
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTimeImmutable());
        }
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
