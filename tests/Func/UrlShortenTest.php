<?php

namespace App\Tests\Func;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UrlShortenTest extends AbstractEndPoint
{
    public function testGetUrls(): array
    {
        $response = $this->getResponseFromRequest(
            Request::METHOD_GET,
            '/api/url_shortens',
            '',
            [],
            true
        );

        $responseContent = $response->getContent();
        $responseDecoded = json_decode($responseContent);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertJson($responseContent);
        self::assertNotEmpty($responseDecoded);

        return $responseDecoded;
    }
}
