<?php

namespace App\Controller;

use App\Repository\UrlShortenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UrlRedirectController extends AbstractController
{
    /**
     * @Route("api/redirect/{shortenedUrl}", methods={"GET"}, name="url_redirect")
     */
    public function UrlRedirect($shortenedUrl, UrlShortenRepository $urlShortenRepository, EntityManagerInterface $em)
    {
        $url = $urlShortenRepository->findOneBy([
            'shortUrl' => $shortenedUrl,
        ]);
        $originalUrl = $url->getFullUrl();
        $url->setClicks($url->getClicks() + 1);
        $em->persist($url);
        $em->flush();
        return $this->redirect($originalUrl);
    }
}
