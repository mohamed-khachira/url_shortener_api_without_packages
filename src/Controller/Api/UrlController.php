<?php

namespace App\Controller\Api;

use App\Entity\UrlShorten;
use App\Repository\UrlShortenRepository;
use App\Service\TokenizedUrlGeneratorService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class UrlController
 * @package App\Controller\Api
 * @Route("/url_shortens")
 */

class UrlController extends ApiController
{
    private $validator;
    private $entityManager;
    private $security;
    private $tokenizedUrlGeneratorService;

    public function __construct(ValidatorInterface $validator, EntityManagerInterface $entityManager, Security $security, TokenizedUrlGeneratorService $tokenizedUrlGeneratorService)
    {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->tokenizedUrlGeneratorService = $tokenizedUrlGeneratorService;
    }

    /**
     * @Route(name="api_urls_collection_post", methods={"POST"})
     */
    public function post(Request $request): JsonResponse
    {
        $user = $this->security->getUser();
        $request = $this->transformJsonBody($request);
        $fullUrl = $request->get('fullUrl');
        try {
            $urlShorten = new UrlShorten();
            $urlShorten->setFullUrl($fullUrl);
            $urlShorten->setUser($user);
            $urlShorten->setShortUrl($this->tokenizedUrlGeneratorService->getRandomToken());
            $errors = $this->validator->validate($urlShorten);
            if (count($errors) > 0) {
                return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
            }
            $this->entityManager->persist($urlShorten);
            $this->entityManager->flush();
            return $this->json($urlShorten, JsonResponse::HTTP_CREATED, [], ['groups' => 'urls_read']);
        } catch (\Exception $e) {
            return $this->json(['status' => JsonResponse::HTTP_BAD_REQUEST, 'message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route(name="api_urls_collection_get", methods={"GET"})
     */
    public function collection(Request $request, UrlShortenRepository $urlShortenRepository): JsonResponse
    {
        $user = $this->security->getUser();
        $current_user_urls = $urlShortenRepository->findBy(['user' => $user]);
        return $this->json($current_user_urls, JsonResponse::HTTP_OK, [], ['groups' => 'urls_read']);
    }

    /**
     * @Route("/{id}", name="api_urls_item_delete", methods={"DELETE"})
     */
    public function delete(UrlShorten $urlShorten): JsonResponse
    {
        $user = $this->security->getUser();
        if ($user === $urlShorten->getUser()) {
            $this->entityManager->remove($urlShorten);
            $this->entityManager->flush();
            return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
        }
        return $this->json(['message' => 'You are not authorized to access this area !'], JsonResponse::HTTP_UNAUTHORIZED);
    }
}
