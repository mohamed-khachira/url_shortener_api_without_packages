<?php

namespace App\Tests\Unit;

use App\Entity\UrlShorten;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
    }

    public function testGetEmail(): void
    {
        $value = 'test@test.de';

        $response = $this->user->setEmail($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getEmail());
        self::assertEquals($value, $this->user->getUsername());
    }

    public function testGetRoles(): void
    {
        $value = ['ROLE_ADMIN'];

        $response = $this->user->setRoles($value);

        self::assertInstanceOf(User::class, $response);
        self::assertContains('ROLE_USER', $this->user->getRoles());
        self::assertContains('ROLE_ADMIN', $this->user->getRoles());
    }

    public function testGetPassword(): void
    {
        $value = 'password';

        $response = $this->user->setPassword($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPassword());
    }

    public function testGetUrlShortens(): void
    {
        $url1 = new UrlShorten();
        $url2 = new UrlShorten();
        $url3 = new UrlShorten();

        $this->user->addUrlShorten($url1);
        $this->user->addUrlShorten($url2);
        $this->user->addUrlShorten($url3);

        self::assertCount(3, $this->user->getUrlShortens());
        self::assertTrue($this->user->getUrlShortens()->contains($url1));
        self::assertTrue($this->user->getUrlShortens()->contains($url2));
        self::assertTrue($this->user->getUrlShortens()->contains($url3));

        $response = $this->user->removeUrlShorten($url1);

        self::assertInstanceOf(User::class, $response);
        self::assertCount(2, $this->user->getUrlShortens());
        self::assertFalse($this->user->getUrlShortens()->contains($url1));
        self::assertTrue($this->user->getUrlShortens()->contains($url2));
        self::assertTrue($this->user->getUrlShortens()->contains($url3));
    }
}
