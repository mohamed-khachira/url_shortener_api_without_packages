import store from "./main";
import axios from "axios";

export default {
  namespaced: true,
  state: {
    token: null,
  },
  getters: {
    isAuthenticated(state) {
      return state.token;
    },
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
  },
  actions: {
    async login({ dispatch }, credentails) {
      store.commit("setLoading", true);
      let response = await axios
        .post("/login_check", credentails)
        .catch((e) => {
          store.commit("setLoading", false);
          console.log(e);
        });

      return dispatch("attempt", response.data.token);
    },
    async attempt({ commit, state }, token) {
      if (token) {
        commit("setToken", token);
      }

      if (!state.token) {
        return;
      }
      store.commit("setLoading", false);
    },
    register(_, form) {
      store.commit("setLoading", true);
      return axios.post("/users", form);
    },
    logout({ commit }) {
      store.commit("setLoading", true);
      localStorage.removeItem("token");
      commit("setToken", null);
      store.commit("setLoading", false);
    },
  },
};
