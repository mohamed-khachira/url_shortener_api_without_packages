<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AuthController
 * @package App\Controller\Api
 * @Route("/users")
 */

class AuthController extends ApiController
{
    private $encoder;
    private $userRepository;
    private $validator;

    public function __construct(UserPasswordEncoderInterface $encoder, UserRepository $userRepository, ValidatorInterface $validator)
    {
        $this->encoder = $encoder;
        $this->userRepository = $userRepository;
        $this->validator = $validator;
    }

    /**
     * @Route(name="api_users_collection_post", methods={"POST"})
     */
    public function register(Request $request): JsonResponse
    {
        $request = $this->transformJsonBody($request);
        $password = $request->get('password');
        $email = $request->get('email');
        $user = new User();
        $user->setEmail($email);
        $hash = $this->encoder->encodePassword($user, $password);
        $user->setPassword($hash);
        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            return $this->json(['errors' => $errors], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user = $this->userRepository->create($user);
        return $this->json($user, Response::HTTP_CREATED);
    }
}
